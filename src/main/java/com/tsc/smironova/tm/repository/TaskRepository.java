package com.tsc.smironova.tm.repository;

import com.tsc.smironova.tm.api.repository.ITaskRepository;
import com.tsc.smironova.tm.model.Project;
import com.tsc.smironova.tm.model.Task;
import com.tsc.smironova.tm.util.ValidationUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return this.tasks;
    }

    @Override
    public List<Task> findAll(Comparator<Task> taskComparator) {
        final List<Task> tasks = new ArrayList<>(this.tasks);
        tasks.sort(taskComparator);
        return tasks;
    }

    @Override
    public int size() {
        return tasks.size();
    }

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId()))
                return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task : tasks) {
            if (name.equals(task.getName()))
                return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null)
            return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> taskList = new ArrayList<>();
        for (Task task : tasks) {
            if (projectId.equals(task.getProjectId()))
                taskList.add(task);
        }
        return taskList;
    }

    @Override
    public List<Task> removeAllByProjectId(final String projectId) {
        for (Task task : tasks) {
            if (projectId.equals(task.getProjectId()))
                task.setProjectId(null);
        }
        return tasks;
    }

    @Override
    public Task bindTaskToProject(final String projectId, final String taskId) {
        final Task task = findOneById(taskId);
        if (task == null)
            return null;
        task.setProjectId(projectId);
        return task;

    }

    @Override
    public Task unbindTaskFromProject(final String taskId) {
        final Task task = findOneById(taskId);
        if (task == null)
            return null;
        task.setProjectId(null);
        return task;
    }

}
