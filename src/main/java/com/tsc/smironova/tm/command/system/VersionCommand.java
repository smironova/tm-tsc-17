package com.tsc.smironova.tm.command.system;

import com.tsc.smironova.tm.command.AbstractCommand;
import com.tsc.smironova.tm.constant.ArgumentConstant;
import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.util.SystemOutUtil;

public class VersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConstant.ARG_VERSION;
    }

    @Override
    public String name() {
        return TerminalConstant.VERSION;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.VERSION;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
        SystemOutUtil.printLine();
    }

}
