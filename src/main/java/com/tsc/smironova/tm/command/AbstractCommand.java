package com.tsc.smironova.tm.command;

import com.tsc.smironova.tm.api.service.ServiceLocator;
import com.tsc.smironova.tm.util.ColorUtil;
import com.tsc.smironova.tm.util.ValidationUtil;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute();

    @Override
    public String toString() {
        String result = "";
        if (!ValidationUtil.isEmpty(name()))
            result += ColorUtil.PURPLE + name() + ColorUtil.RESET + " ";
        if (!ValidationUtil.isEmpty(arg()))
            result += ColorUtil.PURPLE + "(" + arg() + ") " + ColorUtil.RESET;
        if (!ValidationUtil.isEmpty(description()))
            result += "- " + description() + " ";
        return result;
    }

}
