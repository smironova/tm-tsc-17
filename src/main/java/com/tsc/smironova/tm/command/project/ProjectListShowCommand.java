package com.tsc.smironova.tm.command.project;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.enumerated.Sort;
import com.tsc.smironova.tm.exception.empty.EmptyProjectList;
import com.tsc.smironova.tm.exception.system.UnknownSortException;
import com.tsc.smironova.tm.model.Project;
import com.tsc.smironova.tm.util.ColorUtil;
import com.tsc.smironova.tm.util.TerminalUtil;
import com.tsc.smironova.tm.util.ValidationUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListShowCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.PROJECT_LIST;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.PROJECT_LIST;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(ColorUtil.PURPLE + Arrays.toString(Sort.values()) + ColorUtil.RESET);
        final String sort = TerminalUtil.nextLine();
        final List<Project> projects;
        if (ValidationUtil.isEmpty(sort))
            projects = getProjectService().findAll();
        else if (ValidationUtil.checkSort(sort.toUpperCase()))
            throw new UnknownSortException(sort);
        else {
            final Sort sortType = Sort.valueOf(sort.toUpperCase());
            System.out.println(ColorUtil.PURPLE + sortType.getDisplayName() + ColorUtil.RESET);
            projects = getProjectService().findAll(sortType.getComparator());
        }
        if (ValidationUtil.isEmpty(projects))
            throw new EmptyProjectList();
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
