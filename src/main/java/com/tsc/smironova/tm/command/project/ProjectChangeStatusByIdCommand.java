package com.tsc.smironova.tm.command.project;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.enumerated.Status;
import com.tsc.smironova.tm.exception.entity.ProjectNotFoundException;
import com.tsc.smironova.tm.model.Project;
import com.tsc.smironova.tm.util.ColorUtil;
import com.tsc.smironova.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.PROJECT_UPDATE_STATUS_BY_ID;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.PROJECT_UPDATE_STATUS_BY_ID;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(ColorUtil.PURPLE + Arrays.toString(Status.values()) + ColorUtil.RESET);
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = getProjectService().changeProjectStatusById(id, status);
        if (project == null)
            throw new ProjectNotFoundException();
    }

}
