package com.tsc.smironova.tm.command.union;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.TasksNotFoundInProjectException;
import com.tsc.smironova.tm.model.Task;
import com.tsc.smironova.tm.util.TerminalUtil;

import java.util.List;

public class TasksShowAllByProjectIdCommand extends AbstractProjectTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.TASK_LIST_BY_PROJECT_ID;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.TASK_LIST_BY_PROJECT_ID;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getProjectTaskService().findAllTasksByProjectId(projectId);
        if (tasks == null)
            throw new TasksNotFoundInProjectException(projectId);
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
