package com.tsc.smironova.tm.command.project;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.ProjectNotFoundException;
import com.tsc.smironova.tm.model.Project;
import com.tsc.smironova.tm.util.SystemOutUtil;
import com.tsc.smironova.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.PROJECT_UPDATE_BY_ID;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.PROJECT_UPDATE_BY_ID;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        if (project == null) {
            SystemOutUtil.printFailMessage();
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdatedId = getProjectService().updateProjectById(id, name, description);
        if (projectUpdatedId == null)
            throw new ProjectNotFoundException();
    }

}
