package com.tsc.smironova.tm.command.task;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.enumerated.Sort;
import com.tsc.smironova.tm.exception.empty.EmptyTaskList;
import com.tsc.smironova.tm.exception.system.UnknownSortException;
import com.tsc.smironova.tm.model.Task;
import com.tsc.smironova.tm.util.ColorUtil;
import com.tsc.smironova.tm.util.TerminalUtil;
import com.tsc.smironova.tm.util.ValidationUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListShowCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.TASK_LIST;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.TASK_LIST;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(ColorUtil.PURPLE + Arrays.toString(Sort.values()) + ColorUtil.RESET);
        final String sort = TerminalUtil.nextLine();
        final List<Task> tasks;
        if (ValidationUtil.isEmpty(sort))
            tasks = getTaskService().findAll();
        else if (ValidationUtil.checkSort(sort.toUpperCase()))
            throw new UnknownSortException(sort);
        else {
            final Sort sortType = Sort.valueOf(sort.toUpperCase());
            System.out.println(ColorUtil.PURPLE + sortType.getDisplayName() + ColorUtil.RESET);
            tasks = getTaskService().findAll(sortType.getComparator());
        }
        if (ValidationUtil.isEmpty(tasks))
            throw new EmptyTaskList();
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
