package com.tsc.smironova.tm.command.union;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.ProjectNotFoundException;
import com.tsc.smironova.tm.model.Project;
import com.tsc.smironova.tm.util.TerminalUtil;

public class ProjectRemoveWithTasksByIdCommand extends AbstractProjectTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.PROJECT_REMOVE_BY_ID;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.PROJECT_REMOVE_BY_ID;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectTaskService().removeProjectById(id);
        if (project == null)
            throw new ProjectNotFoundException();
        showProject(project);
    }

}
