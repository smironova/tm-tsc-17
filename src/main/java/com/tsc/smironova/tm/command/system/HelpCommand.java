package com.tsc.smironova.tm.command.system;

import com.tsc.smironova.tm.command.AbstractCommand;
import com.tsc.smironova.tm.constant.ArgumentConstant;
import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConstant.ARG_HELP;
    }

    @Override
    public String name() {
        return TerminalConstant.HELP;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.HELP;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands)
            System.out.println(command);
    }

}
