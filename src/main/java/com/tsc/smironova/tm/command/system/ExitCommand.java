package com.tsc.smironova.tm.command.system;

import com.tsc.smironova.tm.command.AbstractCommand;
import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.util.SystemOutUtil;

public class ExitCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.EXIT;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.EXIT;
    }

    @Override
    public void execute() {
        SystemOutUtil.printExitMessage();
        System.exit(0);
    }

}
