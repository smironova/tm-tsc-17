package com.tsc.smironova.tm.command.union;

import com.tsc.smironova.tm.api.service.IProjectTaskService;
import com.tsc.smironova.tm.command.AbstractCommand;
import com.tsc.smironova.tm.exception.entity.ProjectNotFoundException;
import com.tsc.smironova.tm.model.Project;

public abstract class AbstractProjectTaskCommand extends AbstractCommand {

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    protected void showProject(final Project project) {
        if (project == null)
            throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
    }

}
