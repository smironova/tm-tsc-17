package com.tsc.smironova.tm;

import com.tsc.smironova.tm.boostrap.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
