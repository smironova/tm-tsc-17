package com.tsc.smironova.tm.constant;

public class SystemDescriptionConstant {

    public static final String ABOUT = "Display developer info.";
    public static final String ARGUMENTS = "Show program arguments.";
    public static final String COMMANDS = "Show program commands.";
    public static final String EXIT = "Close application.";
    public static final String HELP = "Display list of terminal commands.";
    public static final String INFO = "Display system information.";
    public static final String VERSION = "Display program version.";
    public static final String PROJECT_LIST = "Show project list.";
    public static final String PROJECT_CREATE = "Create new project.";
    public static final String PROJECT_CLEAR = "Clear all projects.";
    public static final String PROJECT_VIEW_BY_ID = "Show project by id.";
    public static final String PROJECT_VIEW_BY_INDEX = "Show project by index.";
    public static final String PROJECT_VIEW_BY_NAME = "Show project by name.";
    public static final String PROJECT_REMOVE_BY_ID = "Remove project by id.";
    public static final String PROJECT_REMOVE_BY_INDEX = "Remove project by index.";
    public static final String PROJECT_REMOVE_BY_NAME = "Remove project by name.";
    public static final String PROJECT_UPDATE_BY_ID = "Update project by id.";
    public static final String PROJECT_UPDATE_BY_INDEX = "Update project by index.";
    public static final String PROJECT_START_BY_ID = "Start project by id.";
    public static final String PROJECT_START_BY_INDEX = "Start project by index.";
    public static final String PROJECT_START_BY_NAME = "Start project by name.";
    public static final String PROJECT_FINISH_BY_ID = "Finish project by id.";
    public static final String PROJECT_FINISH_BY_INDEX = "Finish project by index.";
    public static final String PROJECT_FINISH_BY_NAME = "Finish project by name.";
    public static final String PROJECT_UPDATE_STATUS_BY_ID = "Update project status by id.";
    public static final String PROJECT_UPDATE_STATUS_BY_INDEX = "Update project status by index.";
    public static final String PROJECT_UPDATE_STATUS_BY_NAME = "Update project status by name.";
    public static final String TASK_LIST = "Show task list.";
    public static final String TASK_CREATE = "Create new task.";
    public static final String TASK_CLEAR = "Clear all tasks.";
    public static final String TASK_VIEW_BY_ID = "Show task by id.";
    public static final String TASK_VIEW_BY_INDEX = "Show task by index.";
    public static final String TASK_VIEW_BY_NAME = "Show task by name.";
    public static final String TASK_REMOVE_BY_ID = "Remove task by id.";
    public static final String TASK_REMOVE_BY_INDEX = "Remove task by index.";
    public static final String TASK_REMOVE_BY_NAME = "Remove task by name.";
    public static final String TASK_UPDATE_BY_ID = "Update task by id.";
    public static final String TASK_UPDATE_BY_INDEX = "Update task by index.";
    public static final String TASK_START_BY_ID = "Start task by id.";
    public static final String TASK_START_BY_INDEX = "Start task by index.";
    public static final String TASK_START_BY_NAME = "Start task by name.";
    public static final String TASK_FINISH_BY_ID = "Finish task by id.";
    public static final String TASK_FINISH_BY_INDEX = "Finish task by index.";
    public static final String TASK_FINISH_BY_NAME = "Finish task by name.";
    public static final String TASK_UPDATE_STATUS_BY_ID = "Update task status by id.";
    public static final String TASK_UPDATE_STATUS_BY_INDEX = "Update task status by index.";
    public static final String TASK_UPDATE_STATUS_BY_NAME = "Update task status by name.";
    public static final String TASK_LIST_BY_PROJECT_ID = "Show task list by project id.";
    public static final String TASK_BIND_TO_PROJECT = "Bind task to project.";
    public static final String TASK_UNBIND_FROM_PROJECT = "Unbind task from project.";

}
