package com.tsc.smironova.tm.boostrap;

import com.tsc.smironova.tm.api.repository.ICommandRepository;
import com.tsc.smironova.tm.api.repository.IProjectRepository;
import com.tsc.smironova.tm.api.repository.ITaskRepository;
import com.tsc.smironova.tm.api.service.*;
import com.tsc.smironova.tm.command.AbstractCommand;
import com.tsc.smironova.tm.command.project.*;
import com.tsc.smironova.tm.command.system.*;
import com.tsc.smironova.tm.command.task.*;
import com.tsc.smironova.tm.command.union.*;
import com.tsc.smironova.tm.command.union.ProjectClearCommand;
import com.tsc.smironova.tm.exception.empty.EmptyCommandException;
import com.tsc.smironova.tm.exception.system.UnknownArgumentException;
import com.tsc.smironova.tm.exception.system.UnknownCommandException;
import com.tsc.smironova.tm.repository.CommandRepository;
import com.tsc.smironova.tm.repository.ProjectRepository;
import com.tsc.smironova.tm.repository.TaskRepository;
import com.tsc.smironova.tm.service.*;
import com.tsc.smironova.tm.util.SystemOutUtil;
import com.tsc.smironova.tm.util.TerminalUtil;
import com.tsc.smironova.tm.util.ValidationUtil;

public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new AboutCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new ArgumentsShowCommand());
        registry(new ExitCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectListShowCommand());
        registry(new ProjectRemoveWithTasksByIdCommand());
        registry(new ProjectRemoveWithTasksByIndexCommand());
        registry(new ProjectRemoveWithTasksByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListShowCommand());
        registry(new TasksShowAllByProjectIdCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
    }

    public void run(final String... args) {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        loggerService.debug("!!! TEST !!!");
        if (parseArgs(args))
            new ExitCommand().execute();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                parseCommand(command);
                SystemOutUtil.printOkMessage();
            }
            catch (Exception e) {
                loggerService.error(e);
                SystemOutUtil.printFailMessage();
            }
        }
    }

    public void registry(final AbstractCommand command) {
        if (command == null)
            return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseCommand(final String cmd) {
        if (ValidationUtil.isEmpty(cmd))
            throw new EmptyCommandException();
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null)
            throw new UnknownCommandException(cmd);
        command.execute();
    }

    public void parseArg(final String arg) {
        if (ValidationUtil.isEmpty(arg))
            return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null)
            throw new UnknownArgumentException(arg);
        command.execute();
    }

    public boolean parseArgs(final String[] args) {
        if (ValidationUtil.isEmpty(args))
            return false;
        final String arg = args[0];
        parseArg(arg);
        return  true;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

}
