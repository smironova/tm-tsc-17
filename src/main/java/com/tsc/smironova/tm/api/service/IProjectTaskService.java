package com.tsc.smironova.tm.api.service;

import com.tsc.smironova.tm.model.Project;
import com.tsc.smironova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTasksByProjectId(String projectId);

    Task bindTaskToProject(String projectId, String taskId);

    Task unbindTaskFromProject(String taskId);

    Project removeProjectById(String projectId);

    Project removeProjectByIndex(Integer index);

    Project removeProjectByName(String name);

    void clearProjects();

}
