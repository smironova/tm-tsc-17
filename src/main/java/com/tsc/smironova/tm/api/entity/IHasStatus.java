package com.tsc.smironova.tm.api.entity;

import com.tsc.smironova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
