package com.tsc.smironova.tm.util;

public interface SystemOutUtil {

    static void printOkMessage() {
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        printLine();
    }

    static void printFailMessage() {
        System.err.println("[FAIL]");
        printLine();
    }

    static void printExitMessage() {
        System.out.println(ColorUtil.PURPLE + "[EXIT]" + ColorUtil.RESET);
        printLine();
    }

    static void printLine() {
        System.out.println("--------------------");
    }

}
