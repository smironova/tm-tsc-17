package com.tsc.smironova.tm.util;

import com.tsc.smironova.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            if (ValidationUtil.isEmpty(value))
                throw new IndexIncorrectException();
            else
                throw new IndexIncorrectException(value);
        }
    }

}
