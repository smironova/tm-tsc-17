package com.tsc.smironova.tm.exception.system;

import com.tsc.smironova.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(final String arg) {
        super("Error! Argument ``" + arg + "`` was not found...");
    }

}
